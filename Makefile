migration-generate:
	docker-compose exec api yarn typeorm migration:generate -n $(name) -d src/migrations

migration-revert:
	docker-compose exec api yarn typeorm migration:revert

migration-run:
	docker-compose exec api yarn typeorm migration:run

fixtures:
	docker-compose exec api yarn console:dev db:fixtures:load

db-clean:
	docker-compose exec api yarn console:dev db:clean

.PHONY: migration
