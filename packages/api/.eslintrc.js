module.exports = {
  env: {
      browser: false,
      es6: true,
      node: true
  },
  rules: {
    'prettier/prettier': ['error', {
      tabWidth: 2,
      printWidth: 80,
      singleQuote: true,
      trailingComma: 'es5',
    }],
    '@typescript-eslint/explicit-module-boundary-types': 0
  },
  extends: [
      "plugin:@typescript-eslint/recommended",
      "plugin:@typescript-eslint/recommended-requiring-type-checking",
      "prettier/@typescript-eslint",
      "plugin:prettier/recommended"
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: {
      project: "tsconfig.json",
      sourceType: "module"
  },
  plugins: [
      "@typescript-eslint",
      "@typescript-eslint/tslint"
  ],
};
