import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { CrudConfigService } from '@nestjsx/crud';
import * as Sentry from '@sentry/node';
import { Logger } from 'nestjs-pino';

import { Config } from './config';

CrudConfigService.load({
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    limit: 30,
    maxLimit: 30,
    alwaysPaginate: true,
  },
});

import { AppModule } from './app.module';
import { SentryInterceptor } from './interceptors/sentry.interceptor';

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule);

  Sentry.init({
    dsn: Config.SENTRY_DSN,
    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 1.0,
  });

  if (!Config.IS_DEV) {
    app.useLogger(app.get(Logger));
  }

  app.useGlobalPipes(new ValidationPipe());
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  app.useGlobalInterceptors(new SentryInterceptor());
  app.enableCors();

  const swaggerOptions = new DocumentBuilder()
    .setTitle('SRB API')
    .setVersion('1.0')
    .addBearerAuth({ type: 'apiKey', in: 'header', name: 'Authorization' })
    .build();

  const document = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('api', app, document);

  await app.listen(Config.PORT, '0.0.0.0');
}

void bootstrap();
