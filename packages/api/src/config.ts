import { AuthOptions } from '@nestjsx/crud';
import { User } from './entities/user.entity';

const isDev: boolean = process.env.NODE_ENV !== 'production';

export class Config {
  static readonly IS_DEV: boolean = isDev;

  static readonly PORT: number = parseInt(process.env.PORT || '9000', 10);

  static readonly DATABASE_URL: string = process.env.DATABASE_URL;

  static readonly OKTA_DOMAIN: string = process.env.OKTA_DOMAIN;

  static readonly OKTA_CLIENT_ID: string = process.env.OKTA_CLIENT_ID;

  static readonly OKTA_EVENTS_SECRET: string = process.env.OKTA_EVENTS_SECRET;

  static readonly OKTA_API_TOKEN: string = process.env.OKTA_API_TOKEN;

  static readonly SENTRY_DSN: string = process.env.SENTRY_DSN;

  static readonly FAKER_USER_ID: string = process.env.FAKER_USER_ID;

  static readonly RABBITMQ_REQUESTS_LOGS_QUEUE: string =
    process.env.RABBITMQ_REQUESTS_LOGS_QUEUE;

  static readonly RABBITMQ_URL: string = process.env.RABBITMQ_URL;
}
