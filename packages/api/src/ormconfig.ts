import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Config } from './config';
import { User, Proposal } from './entities';

const ormConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  url: Config.DATABASE_URL,
  entities: [User, Proposal],
  migrations: ['dist/migrations/*{.ts,.js}'],
  migrationsRun: true,
};

export = ormConfig;
