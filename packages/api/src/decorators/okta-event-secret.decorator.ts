import { CustomDecorator, SetMetadata } from '@nestjs/common';

export type EventType =
  | 'user.account.update_profile'
  | 'user.lifecycle.delete.initiated'
  | 'user.lifecycle.create';

export interface EventHookOptions {
  eventType: EventType;
  secret: string;
}

export const OktaEventHook = (
  options: EventHookOptions
): CustomDecorator<string> => SetMetadata('event-hook', options);
