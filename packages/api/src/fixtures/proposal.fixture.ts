import { Proposal, User } from '@/entities';
import { FixtureData } from '@/interfaces/fixture.interface';
import createFixture from '@/utils/create-fixture';
import { lorem, name, random } from 'faker';

export const data: FixtureData<Proposal> = async connection => {
  const owner = (await connection.getRepository(User).find())[0];
  return [
    {
      id: random.uuid(),
      title: name.title(),
      description: lorem.text(),
      owner,
      likes: [],
    },
  ];
};

export default createFixture(Proposal, data);
