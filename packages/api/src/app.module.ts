import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RouterModule } from 'nest-router';
import { ConsoleModule } from 'nestjs-console';
import { LoggerModule } from 'nestjs-pino';

import routes from './routes';
import { HealthModule } from './modules/health/health.module';
import { ProposalsModule } from './modules/api/v1/proposals/proposals.module';
import { CommandsModule } from './modules/commands/commands.module';
import { OktaModule } from './modules/okta/okta.module';
import { SentryInterceptor } from './interceptors/sentry.interceptor';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { UsersModule } from './modules/api/v1/users/users.module';
import ormConfig from './ormconfig';
import { LoggerModuleParams } from './utils/logger-module-params';

@Module({
  imports: [
    // LoggerModule.forRoot(),
    TypeOrmModule.forRoot(ormConfig),
    ConsoleModule,
    CommandsModule,
    RouterModule.forRoutes(routes),
    UsersModule,
    HealthModule,
    ProposalsModule,
    OktaModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: SentryInterceptor,
    },
  ],
  exports: [CommandsModule],
})
export class AppModule {}
