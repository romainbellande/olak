import { Connection } from 'typeorm';

export type FixtureData<T> = (connection: Connection) => Promise<T[]>;

export default interface Fixture<T> {
  name: string;
  type: new () => T;
  data: FixtureData<T>;
}
