import { Jwt } from './jwt.interface';

export default interface OktaJwtVerifier {
  verifyAccessToken(accessToken: string, issuer: string): Promise<Jwt>;
}
