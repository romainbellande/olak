export interface OktaUserEvent {
  eventType: 'com.okta.event_hook';
  eventTypeVersion: string;
  cloudEventsVersion: string;
  source: string;
  eventId: string;
  data: Data;
  eventTime: string; // ex: '2020-10-15T23:09:22.617Z'
  contentType: 'application/json';
}

interface Event {
  uuid: string;
  published: string;
  eventType: string;
  version: string;
  displayMessage: string;
  severity: string;
  client: Client;
  actor: Actor;
  outcome: Outcome;
  target: Actor[];
  transaction: Transaction;
  debugContext: DebugContext;
  legacyEventType: string;
  authenticationContext: AuthenticationContext;
  securityContext: Detail;
}

interface AuthenticationContext {
  authenticationStep: number;
  externalSessionId: string;
}

interface DebugContext {
  debugData: DebugData;
}

interface DebugData {
  requestId: string;
  threatSuspected: string;
  requestUri: string;
  targetEventHookIds: string;
  url: string;
}

interface Transaction {
  type: string;
  id: string;
  detail: Detail;
}

interface Detail {
  [key: string]: string;
}

interface Outcome {
  result: string;
}

export interface Actor {
  id: string;
  type: string;
  alternateId: string;
  displayName: string;
}

interface Client {
  userAgent: UserAgent;
  zone: string;
  device: string;
  ipAddress: string;
  geographicalContext: GeographicalContext;
  ipChain: IpChain[];
}

interface IpChain {
  ip: string;
  geographicalContext: GeographicalContext;
  version: string;
}

interface GeographicalContext {
  city: string;
  state: string;
  country: string;
  postalCode: string;
  geolocation: Geolocation;
}

interface Geolocation {
  lat: number;
  lon: number;
}

interface UserAgent {
  rawUserAgent: string;
  os: string;
  browser: string;
}

interface Data {
  events: Event[];
}
