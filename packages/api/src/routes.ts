import { Routes } from 'nest-router';
import { ProposalsModule } from './modules/api/v1/proposals/proposals.module';
import { UsersModule } from './modules/api/v1/users/users.module';
import { OktaModule } from './modules/okta/okta.module';

const routes: Routes = [
  {
    path: '/api',
    children: [
      {
        path: '/v1',
        children: [
          {
            path: '/proposals',
            module: ProposalsModule,
          },
          {
            path: '/users',
            module: UsersModule,
          },
        ],
      },
    ],
  },
  {
    path: '/okta',
    module: OktaModule,
  },
];

export default routes;
