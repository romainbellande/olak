import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { oktaJwtVerifier } from '@/utils/okta-jwt-verifier';
import { Jwt } from '@/interfaces/jwt.interface';
import { User } from '@/entities';

@Injectable()
export class OktaGuard implements CanActivate {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>
  ) {}
  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request: Request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }

  validateRequest(request: Request): boolean | Promise<boolean> {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const authHeader: string = request.headers['authorization'] || '';
    const match: RegExpExecArray = /Bearer (.+)/.exec(authHeader);

    if (!match) {
      throw new UnauthorizedException();
    }

    const accessToken = match[1];

    return oktaJwtVerifier
      .verifyAccessToken(accessToken, `api://default`)
      .then(async (jwt: Jwt) => {
        const oktaId = jwt.claims.uid;
        if (request.method === 'POST') {
          request['user'] = await this.userRepository.findOne({ oktaId });
        }
        request['oktaId'] = oktaId;
        return true;
      })
      .catch(err => {
        console.error(err);
        throw new UnauthorizedException();
      });
  }
}
