/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';
import { CrudOptions } from '@nestjsx/crud';
import { EntityManager, EntityTarget } from 'typeorm';

import { CRUD_OPTIONS_METADATA } from '@/constants';

@Injectable()
export class OwnerGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private entityManager: EntityManager
  ) {}

  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request: Request = context.switchToHttp().getRequest();
    const options: CrudOptions = this.reflector.get<CrudOptions>(
      CRUD_OPTIONS_METADATA,
      context.getClass()
    );

    return this.validateRequest(request, options.model.type);
  }

  async validateRequest(
    request: Request,
    entityClass: EntityTarget<unknown>
  ): Promise<boolean> {
    const oktaId: string = request['oktaId'] as string;
    const entityId = request['params']['id'] as string;
    const entity = await this.entityManager.findOne(entityClass, entityId);
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    if (entity['owner']['oktaId'] !== oktaId) throw new UnauthorizedException();
    return true;
  }
}
