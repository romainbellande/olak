import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';
import {
  EventHookOptions,
  EventType,
} from '@/decorators/okta-event-secret.decorator';
import { OktaUserEvent } from '@/interfaces/okta-user-event.interface';

@Injectable()
export class OktaEventHookGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    const { eventType, secret } = this.reflector.get<EventHookOptions>(
      'event-hook',
      context.getHandler()
    );

    if (!secret) {
      console.error('no event secret set!');
      return false;
    }

    const request: Request = context.switchToHttp().getRequest();

    return this.validateRequest(request, secret, eventType);
  }

  validateRequest(
    request: Request,
    eventSecret: string,
    eventType: EventType
  ): boolean | Promise<boolean> {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const authHeader: string = request.headers['authorization'] || '';
    const body: OktaUserEvent = (request.body as unknown) as OktaUserEvent;
    return (
      authHeader === eventSecret && body.data.events[0].eventType === eventType
    );
  }
}
