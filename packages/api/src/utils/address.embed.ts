import { ApiResponseProperty } from '@nestjs/swagger';
import { IsEmpty, IsOptional } from 'class-validator';
import { Column } from 'typeorm';

export class Address {
  @ApiResponseProperty()
  @IsEmpty()
  @Column({ nullable: true })
  address1?: string;

  @ApiResponseProperty()
  @IsOptional()
  @Column({ nullable: true })
  address2?: string;

  @ApiResponseProperty()
  @IsOptional()
  @Column({ nullable: true })
  zipcode?: string;

  @ApiResponseProperty()
  @IsOptional()
  @Column({ nullable: true })
  city?: string;

  @ApiResponseProperty()
  @IsOptional()
  @Column({ nullable: true })
  countryCode?: string;

  @ApiResponseProperty()
  @IsOptional()
  @Column({ nullable: true })
  state?: string;
}
