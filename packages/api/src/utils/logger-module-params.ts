import { LoggerModuleAsyncParams, Params } from 'nestjs-pino';
import { Config } from '@/config';
import { AmqpClient } from './amqp-client';

export const LoggerModuleParams: LoggerModuleAsyncParams = {
  useFactory: async () => {
    const params: Params = {
      pinoHttp: {
        level: Config.IS_DEV ? 'debug' : 'info',
        prettyPrint: Config.IS_DEV,
      },
    };

    if (Config.RABBITMQ_URL && Config.RABBITMQ_REQUESTS_LOGS_QUEUE) {
      const amqpClient = new AmqpClient({
        url: Config.RABBITMQ_URL,
        queue: Config.RABBITMQ_REQUESTS_LOGS_QUEUE,
      });

      await amqpClient.setup();
      return {
        ...params,
        pinoHttp: amqpClient,
      };
    } else {
      return params;
    }
  },
};
