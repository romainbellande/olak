/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import IOktaJwtVerifier from 'src/interfaces/okta-jwt-verifier.interface';
import OktaJwtVerifier from '@okta/jwt-verifier';

import { Config } from '@/config';

export const oktaJwtVerifier: IOktaJwtVerifier = new OktaJwtVerifier({
  issuer: `https://${Config.OKTA_DOMAIN}/oauth2/default`, // required
  clientId: Config.OKTA_CLIENT_ID,
});
