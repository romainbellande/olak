import { FixtureData } from '@/interfaces/fixture.interface';
import { EntitySchema, Connection, InsertResult } from 'typeorm';

export const dbInserter = async <T>(
  connection: Connection,
  // eslint-disable-next-line @typescript-eslint/ban-types
  entity: string | Function | EntitySchema<T>,
  fixtureData: FixtureData<T>
): Promise<InsertResult> => {
  const values = await fixtureData(connection);
  return connection
    .createQueryBuilder()
    .insert()
    .into(entity)
    .values(values)
    .execute();
};
