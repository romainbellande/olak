import {
  AmqpConnectionManager,
  ChannelWrapper,
  connect,
} from 'amqp-connection-manager';
import { Channel } from 'amqplib';
import { DestinationStream } from 'pino';
import { Writable } from 'stream';

interface AmqpOptions {
  url: string;
  queue: string;
}

export class AmqpClient extends Writable implements DestinationStream {
  private connection: AmqpConnectionManager;
  private channel: ChannelWrapper;

  constructor(private options: AmqpOptions) {
    super();
  }

  setup(): Promise<void> {
    return new Promise(resolve => {
      this.connection = connect([this.options.url]);
      this.connection.on('connect', () => {
        console.log('connected to rabbitmq host');
        resolve();
      });

      this.connection.on('disconnect', (err: string) => {
        console.error('disconnected to rabbitmq host', err);
      });

      this.channel = this.connection.createChannel({
        json: true,
        setup: (channel: Channel) => channel.checkQueue(this.options.queue),
      });
    });
  }

  _write(chunk: string, enc, next: () => void) {
    void this.channel.sendToQueue(this.options.queue, JSON.parse(chunk));
    next();
  }
}
