import Fixture, { FixtureData } from 'src/interfaces/fixture.interface';

const createFixture = <T>(
  MyClass: { new (): T },
  myData: FixtureData<T>
): Fixture<T> => ({
  name: MyClass.name,
  type: MyClass,
  data: myData,
});

export default createFixture;
