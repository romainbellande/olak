import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiHeader,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { Config } from '@/config';
import { OktaService } from './okta.service';
import { OktaEventHook } from '@/decorators/okta-event-secret.decorator';
import { OktaEventHookGuard } from '@/guards/okta-event-hook.guard';
import { OktaHookVerifierResponse } from './okta-hook-verifier-response';
import { OktaUserEvent } from '@/interfaces/okta-user-event.interface';
import { User } from '@/entities/user.entity';

const extractActorFromUserEvent = (event: OktaUserEvent) =>
  event.data.events[0].target[0];

@ApiTags('okta')
@Controller('')
export class OktaController {
  constructor(public service: OktaService) {}

  @Get('create-user-hook')
  @ApiHeader({
    name: 'X-Okta-Verification-Challenge',
    description: 'Header send by okta to verify the endpoint.',
  })
  @ApiResponse({
    status: 200,
    description: 'verification content',
    type: OktaHookVerifierResponse,
  })
  public createUserHookVerifier(
    @Request() request: Request
  ): OktaHookVerifierResponse {
    return {
      verification: request.headers['x-okta-verification-challenge'] as string,
    };
  }

  @Post('create-user-hook')
  @ApiBearerAuth()
  @ApiResponse({ type: User })
  @UseGuards(OktaEventHookGuard)
  @OktaEventHook({
    eventType: 'user.lifecycle.create',
    secret: Config.OKTA_EVENTS_SECRET,
  })
  public createUserHook(@Body() body: OktaUserEvent): Promise<User> {
    const actor = extractActorFromUserEvent(body);
    return this.service.createUserHook(actor);
  }

  @Get('update-user-hook')
  @ApiHeader({
    name: 'X-Okta-Verification-Challenge',
    description: 'Header send by okta to verify the endpoint.',
  })
  @ApiResponse({
    status: 200,
    description: 'verification content',
    type: OktaHookVerifierResponse,
  })
  public updateUserHookVerifier(
    @Request() request: Request
  ): OktaHookVerifierResponse {
    return {
      verification: request.headers['x-okta-verification-challenge'] as string,
    };
  }

  @Post('update-user-hook')
  @ApiBearerAuth()
  @ApiResponse({ type: User })
  @UseGuards(OktaEventHookGuard)
  @OktaEventHook({
    eventType: 'user.account.update_profile',
    secret: Config.OKTA_EVENTS_SECRET,
  })
  public updateUserHook(@Body() body: OktaUserEvent): unknown {
    const actor = extractActorFromUserEvent(body);
    return this.service.updateUserHook(actor);
  }

  @Get('delete-user-hook')
  @ApiHeader({
    name: 'X-Okta-Verification-Challenge',
    description: 'Header send by okta to verify the endpoint.',
  })
  @ApiResponse({
    status: 200,
    description: 'verification content',
    type: OktaHookVerifierResponse,
  })
  public deleteUserHookVerifier(
    @Request() request: Request
  ): OktaHookVerifierResponse {
    return {
      verification: request.headers['x-okta-verification-challenge'] as string,
    };
  }

  @Post('delete-user-hook')
  @ApiBearerAuth()
  @ApiResponse({ type: User })
  @UseGuards(OktaEventHookGuard)
  @OktaEventHook({
    eventType: 'user.lifecycle.delete.initiated',
    secret: Config.OKTA_EVENTS_SECRET,
  })
  public deleteUserHook(@Body() body: OktaUserEvent): Promise<void> {
    const actor = extractActorFromUserEvent(body);
    return this.service.deleteUserHook(actor);
  }
}
