import { HttpService, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '@/entities/user.entity';
import { DeepPartial, Repository } from 'typeorm';
import { Actor } from '@/interfaces/okta-user-event.interface';
import { UserDto } from '@/entities/user.dto';
import { OktaUser } from './interfaces/okta-user.interface';

@Injectable()
export class OktaService {
  constructor(
    @InjectRepository(User) private repo: Repository<User>,
    private httpService: HttpService
  ) {}

  public createUserHook(actor: Actor): Promise<User> {
    const { id, alternateId, displayName } = actor;

    const userDto: UserDto = {
      oktaId: id,
      email: alternateId,
      displayName,
    };

    return this.repo.save(userDto);
  }

  public async updateUserHook(actor: Actor): Promise<User> {
    const { id, alternateId, displayName } = actor;

    const user = await this.repo.findOne({ oktaId: id });
    user.email = alternateId;
    user.displayName = displayName;

    return this.repo.save(user);
  }

  public async deleteUserHook(actor: Actor): Promise<void> {
    await this.repo.delete({ oktaId: actor.id });
  }

  public async find(): Promise<OktaUser[]> {
    const { data } = await this.httpService
      .get<OktaUser[]>('/users')
      .toPromise();
    return data;
  }

  public async initUsers(): Promise<User[]> {
    const oktaUsers: OktaUser[] = await this.find();
    const users: DeepPartial<User>[] = oktaUsers.map<DeepPartial<User>>(
      oktaUser => ({
        oktaId: oktaUser.id,
        email: oktaUser.profile.email,
        displayName: `${oktaUser.profile.firstName} ${oktaUser.profile.lastName}`,
      })
    );

    return this.repo.save(users);
  }
}
