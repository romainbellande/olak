import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { OktaController } from './okta.controller';
import { OktaService } from './okta.service';
import { User } from '@/entities/user.entity';
import { Config } from '@/config';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    HttpModule.register({
      baseURL: `https://${Config.OKTA_DOMAIN}/api/v1`,
      headers: {
        Authorization: `SSWS ${Config.OKTA_API_TOKEN}`,
      },
    }),
  ],
  controllers: [OktaController],
  providers: [OktaService],
  exports: [OktaService],
})
export class OktaModule {}
