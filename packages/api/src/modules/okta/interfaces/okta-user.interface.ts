export interface OktaUser {
  id: string;
  status: string;
  created: string;
  activated?: any;
  statusChanged?: any;
  lastLogin?: any;
  lastUpdated: string;
  passwordChanged: string;
  profile: Profile;
  credentials: Credentials;
  _links: Links;
}

interface Links {
  self: Self;
}

interface Self {
  href: string;
}

interface Credentials {
  provider: Provider;
}

interface Provider {
  type: string;
  name: string;
}

interface Profile {
  firstName: string;
  lastName: string;
  email: string;
  login: string;
  mobilePhone: string;
}
