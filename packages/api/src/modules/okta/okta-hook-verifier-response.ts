import { ApiResponseProperty } from '@nestjs/swagger';

export class OktaHookVerifierResponse {
  @ApiResponseProperty()
  verification: string;
}
