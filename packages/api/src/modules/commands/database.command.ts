import { Console, Command, createSpinner } from 'nestjs-console';
import { Connection } from 'typeorm';
import sequential from 'promise-sequential';

import { dbInserter } from 'src/utils/db-inserter';
import proposalFixture from 'src/fixtures/proposal.fixture';
import { OktaService } from '../okta/okta.service';
import { Injectable } from '@nestjs/common';

const fixtures = [proposalFixture];

@Injectable()
@Console()
export class DatabaseCommand {
  constructor(
    private readonly connection: Connection,
    private readonly oktaService: OktaService
  ) {}

  @Command({
    command: 'db:init',
    description: 'Initialize database',
  })
  async dbInit(): Promise<void> {
    const spin = createSpinner();
    spin.start(`Initilialize database...`);

    await this.connection.synchronize(true);
    await this.connection.synchronize(false);

    spin.succeed('Initialization done');
  }

  @Command({
    command: 'db:fixtures:load',
    description: 'Initialize database and load fixtures',
  })
  async dbFixtureLoad(): Promise<void> {
    const spin = createSpinner();
    spin.start(`Initilialize database...`);

    await this.cleanAll();

    await this.oktaInit();

    const promises = fixtures.map(fixture => async (): Promise<void> => {
      const fixtureSpinner = createSpinner();
      fixtureSpinner.start(`Loading ${fixture.name} fixture...`);
      await dbInserter(this.connection, fixture.type, fixture.data);
      fixtureSpinner.succeed(`${fixture.name} fixtures loaded`);
    });

    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    await sequential(promises);

    await this.connection.synchronize(false);

    spin.succeed('All fixtures loaded');
  }

  @Command({
    command: 'db:clean',
    description: 'Clean all tables content',
  })
  async cleanAll() {
    const spin = createSpinner();
    spin.start(`Initilialize database...`);
    try {
      for (const entity of this.connection.entityMetadatas) {
        const tableCleanSpinner = createSpinner();
        tableCleanSpinner.start(`clean ${entity.name} table`);
        const repository = this.connection.getRepository(entity.name);
        await repository.query(
          `TRUNCATE TABLE \"${entity.tableName}\" CASCADE;`
        );
        tableCleanSpinner.succeed(`${entity.name} table cleaned`);
      }
      spin.succeed('All tables cleaned');
    } catch (error) {
      spin.fail(`[db:clean] error: ${error as string}`);
      throw new Error(`[db:clean] error: ${error as string}`);
    }
  }

  @Command({
    command: 'okta:init',
    description: 'Initialize user table by fetching Okta users',
  })
  async oktaInit() {
    const spin = createSpinner();
    spin.start(`Initilialize user table...`);

    try {
      await this.oktaService.initUsers();
    } catch (error) {
      spin.fail(`[okta:init] error: ${error as string}`);
      throw new Error(`[okta:init] error: ${error as string}`);
    }

    spin.succeed('All users initialized');
  }
}
