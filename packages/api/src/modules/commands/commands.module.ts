import { Module } from '@nestjs/common';
import { ConsoleModule } from 'nestjs-console';
import { OktaModule } from '../okta/okta.module';

import { DatabaseCommand } from './database.command';

@Module({
  imports: [ConsoleModule, OktaModule],
  controllers: [],
  providers: [DatabaseCommand],
  exports: [DatabaseCommand],
})
export class CommandsModule {}
