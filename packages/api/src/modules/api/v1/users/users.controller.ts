import { Controller } from '@nestjs/common';
import { UseGuards } from '@nestjs/common/decorators/core/use-guards.decorator';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';

import { OktaGuard } from '@/guards/okta.guard';
import { User } from '@/entities/user.entity';
import { UsersService } from './users.service';

@UseGuards(OktaGuard)
@Crud({
  model: { type: User },
  routes: {
    only: ['getManyBase'],
  },
})
@ApiBearerAuth()
@ApiTags('users')
@Controller('')
export class UsersController implements CrudController<User> {
  constructor(public service: UsersService) {}
}
