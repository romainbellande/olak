import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';

import { ProposalsController } from './proposals.controller';
import { ProposalsService } from './proposals.service';
import { MockRepository } from '@/utils/testing/mock-repository';
import { User, Proposal } from '@/entities';
import { entityManagerMock } from '@/utils/testing/entity-manager.mock';

describe('Proposals Controller', () => {
  let controller: ProposalsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProposalsController],
      providers: [
        ProposalsService,
        {
          provide: getRepositoryToken(Proposal),
          useValue: new MockRepository(),
        },
        {
          provide: getRepositoryToken(User),
          useValue: new MockRepository(),
        },
        {
          provide: EntityManager,
          useValue: entityManagerMock,
        },
      ],
    }).compile();

    controller = module.get<ProposalsController>(ProposalsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
