import { ApiProperty } from '@nestjs/swagger';

export class UpdateProposalDto {
  @ApiProperty()
  title?: string;

  @ApiProperty()
  description?: string;
}
