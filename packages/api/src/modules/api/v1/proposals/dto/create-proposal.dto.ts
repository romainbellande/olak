import { ApiProperty } from '@nestjs/swagger';

export class CreateProposalDto {
  @ApiProperty()
  title: string;

  @ApiProperty()
  description: string;
}
