import { Controller, Delete, Param, Post, Req } from '@nestjs/common';
import { UseGuards } from '@nestjs/common/decorators/core/use-guards.decorator';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';

import { Proposal } from '@/entities/proposal.entity';
import { ProposalsService } from './proposals.service';
import { OktaGuard } from '@/guards/okta.guard';
import { Crud, CrudAuth, CrudController, CrudRequest } from '@nestjsx/crud';
import { OwnerGuard } from '@/guards/owner.guard';
import { User } from '@/entities';

@Crud({
  model: { type: Proposal },
  routes: {
    only: ['getManyBase', 'deleteOneBase', 'updateOneBase', 'createOneBase'],
    replaceOneBase: {
      decorators: [UseGuards(OwnerGuard)],
    },
    deleteOneBase: {
      decorators: [UseGuards(OwnerGuard)],
    },
  },
  query: {
    join: {
      owner: {
        eager: true,
      },
    },
  },
})
@CrudAuth({
  property: 'user',
  persist: (user: User) => ({ owner: user }),
})
@ApiBearerAuth()
@UseGuards(OktaGuard)
@ApiTags('proposals')
@Controller('')
export class ProposalsController implements CrudController<Proposal> {
  constructor(public service: ProposalsService) {}

  @ApiResponse({ type: Proposal })
  @Post(':id/like')
  public addLike(@Req() req: CrudRequest, @Param('id') proposalId: string) {
    const oktaId = req['oktaId'] as string;
    return this.service.addLike(oktaId, proposalId);
  }

  @ApiResponse({ type: Proposal })
  @Delete(':id/like')
  public removeLike(@Req() req: CrudRequest, @Param('id') proposalId: string) {
    const oktaId = req['oktaId'] as string;
    return this.service.removeLike(oktaId, proposalId);
  }
}
