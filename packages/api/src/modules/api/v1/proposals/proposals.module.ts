import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ProposalsController } from './proposals.controller';
import { ProposalsService } from './proposals.service';
import { Proposal, User } from '@/entities';

@Module({
  imports: [TypeOrmModule.forFeature([Proposal, User])],
  controllers: [ProposalsController],
  providers: [ProposalsService],
})
export class ProposalsModule {}
