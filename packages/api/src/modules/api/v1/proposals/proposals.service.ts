import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Proposal } from '@/entities/proposal.entity';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { CrudRequest } from '@nestjsx/crud';

@Injectable()
export class ProposalsService extends TypeOrmCrudService<Proposal> {
  constructor(@InjectRepository(Proposal) repo: Repository<Proposal>) {
    super(repo);
  }

  public async addLike(oktaId: string, proposalId: string) {
    const proposal = await this.repo.findOne(proposalId);

    if (!proposal) {
      throw new NotFoundException();
    }

    if (proposal.owner.oktaId === oktaId) {
      throw new ForbiddenException();
    }

    if (proposal.likes.some(like => like === oktaId)) {
      throw new ConflictException();
    }

    const likes = [...proposal.likes, oktaId];

    return this.repo.save({ ...proposal, likes });
  }

  public async removeLike(oktaId: string, proposalId: string) {
    const proposal = await this.repo.findOne(proposalId);

    if (!proposal) {
      throw new NotFoundException();
    }

    if (proposal.owner.oktaId === oktaId) {
      throw new ForbiddenException();
    }

    if (!proposal.likes.some(like => like === oktaId)) {
      throw new ConflictException();
    }

    const likes = proposal.likes.filter(like => like !== oktaId);

    return this.repo.save({ ...proposal, likes });
  }
}
