import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';
import { ApiProperty, ApiResponseProperty } from '@nestjs/swagger';
import { IsEmpty } from 'class-validator';
import { User } from './user.entity';

@Entity()
export class Proposal {
  @ApiProperty()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty()
  @ManyToOne(
    () => User,
    owner => owner.proposals,
    { eager: true, onDelete: 'CASCADE' }
  )
  owner: User;

  @ApiProperty()
  @Column()
  title: string;

  @ApiProperty()
  @Column()
  description: string;

  @ApiResponseProperty()
  @IsEmpty()
  @Column('simple-array', { default: '' })
  likes: string[];

  @ApiProperty()
  @CreateDateColumn()
  createdAt?: string;

  @ApiProperty()
  @UpdateDateColumn()
  updatedAt?: string;
}
