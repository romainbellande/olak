export interface UserDto {
  oktaId: string;
  email: string;
  displayName: string;
}
