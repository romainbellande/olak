/* eslint-disable @typescript-eslint/camelcase */
module.exports = {
  apps: [
    {
      name: 'API',
      script: './dist/main.js',

      // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
      node_args: '-r ./node_modules/tsconfig-paths/register',
      error_file: '/dev/null',
      out_file: '/dev/null',
      log_file: '/dev/null',
      time: true,
      instances: 'max',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
  ],
};
