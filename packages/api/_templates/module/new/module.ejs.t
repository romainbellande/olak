---
  to: src/modules/api/v1/<%= name %>/<%= name %>.module.ts
---
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { <%= h.pascalCase(name) %>Controller } from './<%= name %>.controller';
import { <%= h.pascalCase(name) %>Service } from './<%= name %>.service';
import { <%= h.pascalCase(entity) %> } from '@/entities/<%= entity %>.entity';

@Module({
  imports: [TypeOrmModule.forFeature([<%= h.pascalCase(entity) %>])],
  controllers: [<%= h.pascalCase(name) %>Controller],
  providers: [<%= h.pascalCase(name) %>Service],
})
export class <%= h.pascalCase(name) %>Module {}
