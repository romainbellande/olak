---
  to: src/modules/api/v1/<%= name %>/<%= name %>.service.ts
---
import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { <%= h.pascalCase(entity) %> } from '@/entities/<%= entity %>.entity';
import { Repository } from 'typeorm';

@Injectable()
export class <%= h.pascalCase(name) %>Service extends TypeOrmCrudService<<%= h.pascalCase(entity) %>> {
  constructor(@InjectRepository(<%= h.pascalCase(entity) %>) repo: Repository<<%= h.pascalCase(entity) %>>) {
    super(repo);
  }
}
