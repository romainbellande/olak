---
  to: src/entities/<%= entity %>.entity.ts
---
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ApiResponseProperty } from '@nestjs/swagger';

@Entity()
export class <%= h.pascalCase(entity) %> {
  @ApiResponseProperty()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiResponseProperty()
  @CreateDateColumn()
  createdAt?: Date;

  @ApiResponseProperty()
  @UpdateDateColumn()
  updatedAt?: Date;
}
