
module.exports = [
  {
    type: 'numeral',
    name: 'version',
    message: "What's your module version?",
    initial: 1,
  },
  {
    type: 'input',
    name: 'name',
    message: "What's your module name?"
  },
  {
    type: 'input',
    name: 'entity',
    message: "What's your module entity?"
  },
];
