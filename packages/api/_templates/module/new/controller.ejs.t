---
  to: src/modules/api/v1/<%= name %>/<%= name %>.controller.ts
---
import { Controller } from '@nestjs/common';
import { UseGuards } from '@nestjs/common/decorators/core/use-guards.decorator';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudAuth, CrudController } from '@nestjsx/crud';

import { Config } from '@/config';
import { OktaGuard } from '@/guards/okta.guard';
import { <%= h.pascalCase(entity) %> } from '@/entities/<%= entity %>.entity';
import { <%= h.pascalCase(name) %>Service } from './<%= name %>.service';

@UseGuards(OktaGuard)
@Crud({
  model: { type: <%= h.pascalCase(entity) %> },
})
@CrudAuth(Config.DEFAULT_CRUD_AUTH_OPTIONS)
@ApiBearerAuth()
@ApiTags('<%= name %>')
@Controller('')
export class <%= h.pascalCase(name) %>Controller implements CrudController<<%= h.pascalCase(entity) %>> {
  constructor(public service: <%= h.pascalCase(name) %>Service) {}
}
