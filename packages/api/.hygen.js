const { pascalCase } = require('change-case');

module.exports = {
  helpers: {
    pascalCase,
  }
}
