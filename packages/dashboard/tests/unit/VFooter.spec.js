import '@testing-library/jest-dom';

import VFooter from '@/components/VFooter.vue';
import renderWithVuetify from './renderWithVuetify';

describe('VFooter.vue', () => {
  it('renders props.msg when passed', () => {
    const { getByTestId } = renderWithVuetify(VFooter);
    const footer = getByTestId('footer');
    expect(footer).toBeTruthy();
  });
});
