module.exports = {
  transpileDependencies: ['vuetify'],
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      // mutate config for production...
    } else {
      config.devServer = {
        disableHostCheck: true,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': '*',
          'Access-Control-Allow-Methods':
            'GET, POST, PUT, DELETE, PATCH, OPTIONS',
        },
      };
    }
  },
  chainWebpack: config => {
    config.plugins.delete('prefetch');
  },
};
