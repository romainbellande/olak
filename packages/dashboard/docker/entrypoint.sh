#!/usr/bin/env sh

app_env='window.env = {'

for pair in $(env | grep VUE_APP); do
    key=$(echo $pair | cut -d= -f1)
    value=$(echo $pair | cut -d= -f2)
    app_env=''"${app_env}"' "'"${key}"'":"'"${value}"'",'
done

app_env="${app_env}}"

sed -i "s~// ___ENV___~${app_env}~" /usr/share/nginx/html/index.html

exec "$@"
