import camelCaseSerializer from '@/utils/camelCaseSerializer';

export default {
  namespaced: true,
  state: {
    user: {
      email: '',
      emailVerified: '',
      familyName: '',
      givenName: '',
      locale: '',
      name: '',
      preferredUsername: '',
      sub: '',
      updatedAt: null,
      zoneinfo: '',
    },
  },
  getters: {
    user: state => state.user,
    externalId: state => state.user.sub,
  },
  actions: {},
  mutations: {
    setUser(state, user) {
      state.user = camelCaseSerializer(user);
    },
  },
};
