import Vue from 'vue';
import replaceResourceInCollection from '@/utils/replaceResourceInCollection';

export default {
  namespaced: true,
  state: {
    proposals: null,
  },
  getters: {
    proposals: state => state.proposals,
    proposalsByOwnerId: state => ownerId =>
      (state.proposals || []).filter(
        proposal => proposal.owner.oktaId === ownerId
      ),
    proposalsNotOwnerId: state => ownerId =>
      (state.proposals || []).filter(
        proposal => proposal.owner.oktaId !== ownerId
      ),
  },
  mutations: {
    setProposals(state, proposals = []) {
      state.proposals = proposals;
    },
    addProposal(state, proposal) {
      state.proposals.push(proposal);
    },
    replaceProposal(state, proposal) {
      state.proposals = replaceResourceInCollection(state.proposals, proposal);
    },
    deleteProposal(state, id) {
      state.proposals = state.proposals.filter(proposal => proposal.id !== id);
    },
  },
  actions: {
    async fetchProposals({ commit }) {
      const { data } = await Vue.axios.get('/proposals');
      commit('setProposals', data.data);
    },

    async fetchMyProposals({ commit, rootState }) {
      const oktaId = rootState.user.user.sub;
      const params = new URLSearchParams([
        ['filter', `owner.oktaId||$eq||${oktaId}`],
      ]);
      const { data } = await Vue.axios.get('/proposals', { params });
      commit('setMyProposals', data.data);
    },

    async patchProposal({ commit }, { id, ...body }) {
      const { data } = await Vue.axios.patch(`/proposals/${id}`, body);
      commit('replaceProposal', data);
    },

    async deleteProposal({ commit }, id) {
      await Vue.axios.delete(`/proposals/${id}`);
      commit('deleteProposal', id);
    },

    async createProposal({ commit }, body) {
      const { data } = await Vue.axios.post('/proposals', body);
      commit('addProposal', data);
    },

    async likeProposal({ commit }, id) {
      const { data } = await Vue.axios.post(`/proposals/${id}/like`);
      commit('replaceProposal', data);
    },

    async unlikeProposal({ commit }, id) {
      const { data } = await Vue.axios.delete(`/proposals/${id}/like`);
      commit('replaceProposal', data);
    },
  },
};
