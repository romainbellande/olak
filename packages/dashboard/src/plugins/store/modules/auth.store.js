import Vue from 'vue';

export default {
  namespaced: true,
  state: {
    token: null,
  },
  getters: {},
  actions: {},
  mutations: {
    setToken(state, token) {
      Vue.axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
      state.token = token;
    },
  },
};
