import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth.store';
import user from './modules/user.store';
import proposals from './modules/proposals.store';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    user,
    proposals,
  },
});
