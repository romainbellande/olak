import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import themes from './themes';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdiSvg', // default - only for display purposes
  },
  theme: {
    options: {
      customProperties: true,
    },
    themes: themes.default,
  },
});
