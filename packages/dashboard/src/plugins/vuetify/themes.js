import colors from 'vuetify/lib/util/colors';

export default {
  default: {
    light: {
      primary: colors.lightBlue.darken4,
    },
  },
};
