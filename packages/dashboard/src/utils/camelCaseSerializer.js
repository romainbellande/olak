import { camelCase } from 'lodash';

export default obj =>
  Object.entries(obj)
    .map(([key, value]) => ({ [camelCase(key)]: value }))
    .reduce((prev, next) => ({ ...prev, ...next }), {});
