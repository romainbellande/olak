export default (array = [], resource = {}) => {
  if (!resource.id) throw new Error(`resource doesn't have id`);
  return [...array.filter(item => item.id !== resource.id), resource];
};
