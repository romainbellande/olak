import { OKTA_DOMAIN, OKTA_CLIENT_ID, IS_DEV } from '@/env';

export const CALLBACK_PATH = '/implicit/callback';

const ISSUER = `https://${OKTA_DOMAIN}/oauth2/default`;
const MY_HOST = window.location.host;
const REDIRECT_URI = `${window.location.protocol}//${MY_HOST}${CALLBACK_PATH}`;
const POST_LOGOUT_REDIRECT_URI = '/login';
const SCOPES = 'openid profile email';

export const logoutRedirect = idToken =>
  `${ISSUER}/v1/logout?id_token_hint=${idToken}&post_logout_redirect_uri=${window.location.protocol}//${MY_HOST}${POST_LOGOUT_REDIRECT_URI}`;

export const config = {
  issuer: ISSUER,
  clientId: OKTA_CLIENT_ID,
  redirectUri: REDIRECT_URI,
  scope: SCOPES.split(/\s+/),
  secure: !IS_DEV,
  testing: {
    disableHttpsCheck: IS_DEV,
  },
};

export const signInWidgetConfig = {
  baseUrl: `https://${OKTA_DOMAIN}`,
  clientId: OKTA_CLIENT_ID,
  redirectUri: REDIRECT_URI,
  authParams: {
    pkce: true,
    issuer: ISSUER,
    display: 'page',
    scopes: ['openid', 'profile', 'email'],
  },
};
