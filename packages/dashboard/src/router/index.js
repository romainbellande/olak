import Vue from 'vue';
import VueRouter from 'vue-router';
import Auth from '@okta/okta-vue';

import Dashboard from '@/views/Dashboard.vue';
import { CALLBACK_PATH, config as oktaConfig } from '@/utils/okta';

Vue.use(Auth, { ...oktaConfig });
Vue.use(VueRouter);

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login'),
  },
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
    meta: { requiresAuth: true },
    children: [
      {
        path: '',
        component: () => import('@/views/Home'),
      },
      {
        path: '/proposals',
        component: () => import('@/views/Proposals'),
        children: [
          {
            path: '',
            name: 'proposals',
            component: () => import('@/views/Proposals/Proposals'),
          },
          {
            path: 'my-proposals',
            name: 'my-proposals',
            component: () => import('@/views/Proposals/MyProposals'),
          },
        ],
      },
    ],
  },
  {
    path: CALLBACK_PATH,
    component: Auth.handleCallback(),
  },
  {
    path: '*',
    name: 'NotFound',
    component: () => import('@/views/NotFound'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

const onAuthRequired = async (from, to, next) => {
  if (
    from.matched.some(record => record.meta.requiresAuth) &&
    !(await Vue.prototype.$auth.isAuthenticated())
  ) {
    // Navigate to custom login page
    next({ path: '/login' });
  } else {
    next();
  }
};

router.beforeEach(onAuthRequired);

export default router;
