import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

import './plugins/sentry';
import vuetify from './plugins/vuetify';
import App from './App.vue';

import router from './router';
import store from './plugins/store';
import { API_URL } from './env';

Vue.config.productionTip = false;
Vue.use(VueAxios, axios);
Vue.axios.defaults.baseURL = API_URL;

Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App),
}).$mount('#app');
