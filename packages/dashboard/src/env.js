const env = (envName, defaultEnv) =>
  (window.env && window.env[envName]) || process.env[envName] || defaultEnv;

export const OKTA_DOMAIN = env('VUE_APP_OKTA_DOMAIN');
export const OKTA_CLIENT_ID = env('VUE_APP_OKTA_CLIENT_ID');
export const SENTRY_DSN = env('VUE_APP_SENTRY_DSN');
export const API_URL = env('VUE_APP_API_URL');
export const IS_DEV = process.NODE_ENV !== 'production';
